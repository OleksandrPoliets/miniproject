export default [
  '/auth/login',
  '/auth/register',
  '/password/set-password',
  '/password/reset'
];
