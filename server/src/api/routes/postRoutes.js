import { Router } from 'express';
import * as postService from '../services/postService';

const router = Router();

router
  .get('/', (req, res, next) => postService.getPosts(req.query)
    .then(posts => res.send(posts))
    .catch(next))
  .get('/:id', (req, res, next) => postService.getPostById(req.params.id)
    .then(post => res.send(post))
    .catch(next))
  .get('/likers/:postId', (req, res, next) => postService.getLikers(req.params.postId)
    .then(post => res.send(post))
    .catch(next))
  .post('/', (req, res, next) => postService.create(req.user.id, req.body)
    .then(post => {
      req.io.emit('new_post', post); // notify all users that a new post was created
      return res.send(post);
    })
    .catch(next))
  .post('/share-post', (req, res, next) => postService.sharePostByEmail(req.user.id, req.body)
    .then(post => res.send(post))
    .catch(next))
  .put('/react', (req, res, next) => postService.setReaction(req.user.id, req.body)
    .then(reaction => {
      if (reaction.post && (reaction.post.userId !== req.user.id)) {
        if (reaction.isLike) {
          req.io.to(reaction.post.userId).emit('like', 'Your post was liked!');
        } else {
          req.io.to(reaction.post.userId).emit('dislike', 'Your post was Disliked!');
        }
        // notify a user if someone (not himself) liked his post
      }
      return res.send(reaction);
    })
    .catch(next))
  .put('/edit/:id', (req, res, next) => postService.editPostById(req.user.id, req.body)
    .then(post => {
      const { id, userId } = post;

      if (id) {
        req.io.to(userId).emit('edit', 'Your post was successfully edited');
        return res.send(post);
      }
      return res.status(403)
        .json({
          error: true,
          message: 'You have not permission to edit this Post'
        });
    })
    .catch(next))
  .delete('/delete/:id', (req, res, next) => postService.deletePostById(req)
    .then(reaction => {
      const { isDelete, userId } = reaction;
      if (isDelete) {
        req.io.to(userId).emit('delete', 'Your post was successfully deleted');
        return res.send(isDelete);
      }
      return res.status(403)
        .json({
          error: true,
          message: 'You have not permission to delete this Post'
        });
    })
    .catch(next));
export default router;
