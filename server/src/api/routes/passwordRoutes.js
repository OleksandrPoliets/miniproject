import { Router } from 'express';
import * as passwordServices from '../services/passwordServices';

const router = Router();

router
  .put('/reset', (req, res, next) => passwordServices.sendResetLink(req.body.email)
    .then(data => res.send(data))
    .catch(next))
  .put('/set-password', (req, res, next) => passwordServices.setNewPassword(req.body)
    .then(data => res.send(data))
    .catch(next));

export default router;
