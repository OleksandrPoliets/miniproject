import { Router } from 'express';
import * as authService from '../services/authService';
import * as userService from '../services/userService';
import authenticationMiddleware from '../middlewares/authenticationMiddleware';
import registrationMiddleware from '../middlewares/registrationMiddleware';
import jwtMiddleware from '../middlewares/jwtMiddleware';

const router = Router();

// user added to the request (req.user) in a strategy, see passport config
router
  .post('/login', authenticationMiddleware, (req, res, next) => authService.login(req.user)
    .then(data => res.send(data))
    .catch(next))
  .post('/register', registrationMiddleware, (req, res, next) => authService.register(req.user)
    .then(data => res.send(data))
    .catch(next))
  .get('/user', jwtMiddleware, (req, res, next) => userService.getUserById(req.user.id)
    .then(data => res.send(data))
    .catch(next))
  .post('/user', jwtMiddleware, (req, res, next) => userService
    .updateUserField(req.user.id, req.body)
    .then(data => {
      if (data.id) {
        req.io.to(data.id).emit('update', 'Your profile was updated!');
        return res.send(data);
      }
      return res.status(403)
        .json({
          error: true,
          message: 'Existing username'
        });
    })
    .catch(next));

export default router;
