import userRepository from '../../data/repositories/userRepository';

export const getUserById = async userId => {
  const { id, username, email, imageId, image, status } = await userRepository.getUserById(userId);
  return { id, username, email, imageId, image, status };
};

export const updateUserField = async (userId, data) => {
  const checkName = await userRepository.getByUsername(data.username);
  const tempUser = await userRepository.getUserById(userId);
  let result = {};
  if (!checkName || checkName.username === tempUser.username) {
    await userRepository.updateById(userId, data);
    const { id, username, email, imageId, image, status } = await getUserById(userId);
    result = { id, username, email, imageId, image, status };
  }

  return result;
};

