import userRepository from '../../data/repositories/userRepository';
import { sendMail } from '../../helpers/emailHelper';
import { createToken, decodeToken } from '../../helpers/tokenHelper';
import { encrypt } from '../../helpers/cryptoHelper';

export const sendResetLink = async email => {
  const EMAILVALIDATOR = /^[-._a-z0-9]+@(?:[a-z0-9][-a-z0-9]+\.)+[a-z]{2,6}$/;
  const validEmail = EMAILVALIDATOR.test(email);
  let result;
  if (validEmail) {
    const { id } = await userRepository.getByEmail(email) || false;
    if (id) {
      const token = createToken({ id });
      await userRepository.updateById(id, { token });
      await sendMail(email, token);
      result = { ok: true };
    } else {
      result = { error: 'User not Found' };
    }
  } else {
    result = { error: 'Wrong Email' };
  }
  return result;
};

export const setNewPassword = async ({ password, token }) => {
  let message;
  if (password.length > 5) {
    const { id } = decodeToken(token);
    const passHash = await encrypt(password);
    await userRepository.updateById(id, { password: passHash, passwordToken: null });
    message = { ok: true };
  } else {
    message = { error: 'password length' };
  }
  return message;
};
