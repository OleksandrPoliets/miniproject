import postRepository from '../../data/repositories/postRepository';
import userRepository from '../../data/repositories/userRepository';
import postReactionRepository from '../../data/repositories/postReactionRepository';
import { sendMail } from '../../helpers/emailHelper';

export const getPosts = filter => postRepository.getPosts(filter);

export const getPostById = id => postRepository.getPostById(id);

export const create = (userId, post) => postRepository.create({
  ...post,
  userId
});

async function sendNotificationToPostOwner(postId, userId) {
  const { user } = await postRepository.getPostById(postId);
  const { email } = await userRepository.getUserById(user.id);
  const { id, username } = await userRepository.getUserById(userId);
  return user.id !== id ? sendMail(email, username, 'like') : null;
}

export async function sharePostByEmail(userId, body) {
  const user = await userRepository.getUserById(userId);
  return sendMail(body.to, {
    username: user.username,
    postLink: body.postLink
  },
  'sharePost');
}

export const setReaction = async (userId, { postId, isLike = false, isDislike = false }) => {
  // define the callback for future use as a promise
  let prevReaction = false;
  const updateOrDelete = react => {
    if (!isLike) {
      prevReaction = react.isLike;
    } else {
      prevReaction = react.isDislike;
    }

    if (react.isLike === isLike || react.dislike === isDislike) {
      return postReactionRepository.deleteById(react.id);
    }
    return postReactionRepository.updateById(react.id, { isLike, isDislike });
  };
  const reaction = await postReactionRepository.getPostReaction(userId, postId);

  const result = reaction
    ? await updateOrDelete(reaction)
    : await postReactionRepository.create({ userId, postId, isLike, isDislike });

  // the result is an integer when an entity is deleted
  const message = async () => {
    const tempRes = await postReactionRepository.getPostReaction(userId, postId);
    return { ...tempRes.dataValues, prevReaction };
  };
  if (!Number.isInteger(result)) {
    if (result.isLike) {
      sendNotificationToPostOwner(postId, userId);
    }
  }
  return Number.isInteger(result) ? { prevReaction } : message();
};

export const getLikers = async postId => postReactionRepository.getLikers(postId);

export const deletePostById = async param => {
  const { id: userId } = param.user;
  const { id: postId } = param.params;
  const { userId: postUser } = await getPostById(postId);
  let isDelete = false;

  if (postUser === userId) {
    const reaction = await postRepository.deleteById(postId);
    isDelete = Number.isInteger(reaction);
  }

  return { isDelete, userId };
};

export const editPostById = async (userId, newPost) => {
  const { id, body } = newPost;
  const post = await getPostById(id);
  let result;
  if (post.user.id === userId) {
    result = await postRepository.updateById(id, { body });
  } else {
    result = { id: null };
  }
  return result;
};
