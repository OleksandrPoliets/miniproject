import { user, pass } from '../config/mailConfig';

const nodemailer = require('nodemailer');

const resetTemplate = (sendTo, payload) => ({
  from: 'Thread',
  to: sendTo,
  subject: 'Do you want to reset your password?',
  html: ` So you forgot your password. I help You!
            <p>Give answers for this questions</p>
            <ol>
                <li>Your favorite color</li>
                <li>Your Pet name</li>
                <li>Your blood type</li>
                <li>I'll pick a number one to 20 and you will guess</li>
            </ol>
            I'm just kidding Follow next link if you might change your current password.
          <i>Referer link:</i>
          <a href=http://localhost:3000/resetPassword/${payload}
          target='_blank' noopen noreferer> Change your password </a>`
});

const likeTemplate = (sendTo, payload) => ({
  from: 'Thread',
  to: sendTo,
  subject: 'Your post was liked!',
  html: `Your post  was liked by user ${payload}.`
});
const shareTemplate = (sendTo, payload) => ({
  from: 'Thread',
  to: sendTo,
  subject: 'Someone\'s shared post with you!',
  html: `User ${payload.username} send
        <a href=${payload.postLink} target="_blank noopen noreferer"> Post </a> to you`
});

export const sendMail = async (sendTo, payload, type = 'password') => {
  const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user,
      pass
    }
  });
  let body;
  switch (true) {
    case type === 'password':
      body = resetTemplate(sendTo, payload);
      break;
    case type === 'like':
      body = likeTemplate(sendTo, payload);
      break;
    case type === 'sharePost':
      body = shareTemplate(sendTo, payload);
      break;
    default:
      break;
  }
  const result = await transporter.sendMail(body);
  return result;
};
