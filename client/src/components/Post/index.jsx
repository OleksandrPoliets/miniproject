import React from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon, Popup } from 'semantic-ui-react';
import moment from 'moment';
import ReactionComponent from '../ReactionComponent';
import AddPost from '../AddPost';
import styles from './styles.module.scss';
import { addPost } from '../../containers/Thread/actions';

const Post = ({
  post,
  likePost,
  dislikePost,
  toggleExpandedPost,
  sharePost,
  onHover,
  showLikeList,
  userId,
  deletePost,
  addPostToEdit,
  postToEdit,
  uploadImage,
  editUserPost,
  cancelUpdate
}) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt,
    likers
  } = post;
  const { show = false, postId = false } = showLikeList;
  const date = moment(createdAt).fromNow();
  return postToEdit === id
    ? (
      <AddPost
        post={post}
        uploadImage={uploadImage}
        addPost={addPost}
        cancelUpdate={cancelUpdate}
        editUserPost={editUserPost}
      />
    )
    : (
      <Card style={{ width: '100%', position: 'relative' }}>
        {image && <Image src={image.link} wrapped ui={false} />}
        <Card.Content>
          <Card.Meta>
            <span className="date">
              posted by
              {' '}
              {user.username}
              {' - '}
              {date}
            </span>
          </Card.Meta>
          <Card.Description>
            {body}
          </Card.Description>
        </Card.Content>
        <Card.Content extra>
          <Label
            basic
            size="small"
            as="a"
            className={styles.toolbarBtn}
            onMouseEnter={onHover ? e => onHover(e, post) : null}
            onMouseLeave={onHover ? e => onHover(e) : null}
            onClick={() => likePost(id)}
          >
            <Icon name="thumbs up" />
            {likeCount}
          </Label>
          <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikePost(id)}>
            <Icon name="thumbs down" />
            {dislikeCount}
          </Label>
          {
            userId && (
              <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
                <Icon name="comment" />
                {commentCount}
              </Label>
            )
          }
          <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
            <Icon name="share alternate" />
          </Label>
          {userId === user.id && (
            <>
              <Popup
                trigger={(
                  <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => addPostToEdit(id)}>
                    <Icon name="edit" />
                  </Label>
                )}
                content="Edit Post"
                position="bottom center"
              />
              <Popup
                trigger={(
                  <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => deletePost(id)}>
                    <Icon name="trash alternate" />
                  </Label>
                )}
                content="Delet Post"
                position="bottom center"
              />
            </>
          )}
        </Card.Content>
        {show && postId === id && (
          <ReactionComponent
            userReactionList={likers}
          />
        )}
      </Card>
    );
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  onHover: PropTypes.func,
  showLikeList: PropTypes.objectOf(PropTypes.any).isRequired,
  userId: PropTypes.string,
  deletePost: PropTypes.func,
  addPostToEdit: PropTypes.func,
  postToEdit: PropTypes.string,
  uploadImage: PropTypes.func,
  editUserPost: PropTypes.func,
  cancelUpdate: PropTypes.func
};

Post.defaultProps = {
  userId: '',
  postToEdit: '',
  deletePost: undefined,
  addPostToEdit: undefined,
  onHover: undefined,
  uploadImage: undefined,
  editUserPost: undefined,
  cancelUpdate: undefined
};

export default Post;
