import React from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Label, Icon } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';

const Comment = ({ comment: { body, createdAt, user } }) => (
  <CommentUI className={styles.comment}>
    <CommentUI.Avatar src={getUserImgLink(user.image)} />
    <CommentUI.Content>
      <CommentUI.Author as="a">
        {user.username}
      </CommentUI.Author>
      <CommentUI.Metadata>
        {moment(createdAt).fromNow()}
      </CommentUI.Metadata>
      {user.status && (
        <CommentUI.Content>
          <CommentUI.Metadata>
            {user.status}
          </CommentUI.Metadata>
        </CommentUI.Content>
      )}
      <CommentUI.Text>
        {body}
      </CommentUI.Text>
      <Label
        basic
        size="small"
        as="a"
        // onClick={() => likeComment(id)}
      >
        <Icon name="thumbs up" />
        <span>
          1
        </span>
      </Label>
      <Label basic size="small" as="a">
        <Icon name="thumbs down" />
        0
      </Label>
    </CommentUI.Content>
  </CommentUI>
);

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired
};

export default Comment;
