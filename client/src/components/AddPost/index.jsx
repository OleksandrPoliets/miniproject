import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Icon, Image, Segment } from 'semantic-ui-react';

import styles from './styles.module.scss';

const AddPost = ({
  addPost,
  uploadImage,
  post,
  cancelUpdate,
  editUserPost
}) => {
  const [body, setBody] = useState('');
  const [image, setImage] = useState(undefined);
  const [isUploading, setIsUploading] = useState(false);

  useEffect(() => {
    if (post && post.body) setBody(post.body);
  }, [post]);
  const handleAddPost = async () => {
    if (!body) {
      return;
    }
    if (post && post.id) {
      await editUserPost({ id: post.id, imageId: image?.imageId, body });
      cancelUpdate();
    } else {
      await addPost({ imageId: image?.imageId, body });
      setBody('');
      setImage(undefined);
    }
  };
  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const { id: imageId, link: imageLink } = await uploadImage(target.files[0]);
      setImage({ imageId, imageLink });
    } finally {
      // TODO: show error
      setIsUploading(false);
    }
  };

  return (
    <Segment>
      <Form onSubmit={handleAddPost}>
        <Form.TextArea
          name="body"
          value={body}
          placeholder="What is the news?"
          onChange={ev => setBody(ev.target.value)}
        />
        {image?.imageLink && (
          <div className={styles.imageWrapper}>
            <Image className={styles.image} src={image?.imageLink} alt="post" />
          </div>
        )}
        <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
          <Icon name="image" />
          Attach image
          <input name="image" type="file" onChange={handleUploadFile} hidden />
        </Button>
        {post && post.body && (
          <Button floated="right" color="red" onClick={cancelUpdate} type="reset">
            Cancel
          </Button>
        )}
        <Button floated="right" color="blue" type="submit">
          {post && post.body ? 'Save' : 'Post' }
        </Button>
      </Form>
    </Segment>
  );
};

AddPost.propTypes = {
  addPost: PropTypes.func.isRequired,
  post: PropTypes.objectOf(PropTypes.any),
  uploadImage: PropTypes.func.isRequired,
  cancelUpdate: PropTypes.func,
  editUserPost: PropTypes.func
};
AddPost.defaultProps = {
  post: {},
  cancelUpdate: undefined,
  editUserPost: undefined
};

export default AddPost;
