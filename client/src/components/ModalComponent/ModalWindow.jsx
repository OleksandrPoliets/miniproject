import React from 'react';
import PropTypes from 'prop-types';
import { Button, Modal } from 'semantic-ui-react';

const ModalWindow = ({ close, confirm }) => (
  <Modal
    size="mini"
    open
    onClose={close}
    className="asdasd"
  >
    <Modal.Header>Confirmation Message</Modal.Header>
    <Modal.Content>
      Are you sure you want to delete this post?
    </Modal.Content>
    <Modal.Actions>
      <Button negative onClick={close}>
        Disagree
      </Button>
      <Button positive onClick={confirm}>
        Agree
      </Button>
    </Modal.Actions>
  </Modal>
);

ModalWindow.propTypes = {
  close: PropTypes.func.isRequired,
  confirm: PropTypes.func.isRequired
};

export default ModalWindow;
