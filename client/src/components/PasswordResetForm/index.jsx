import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Segment } from 'semantic-ui-react';

const PasswordResetForm = ({ reset, resetPassword, setPassword }) => {
  const [email, setEmail] = useState('');
  const [passwordFilds, setPasswordFilds] = useState({
    firstPassword: '',
    secondPassword: ''
  });
  const onSubmit = async () => {
    if (resetPassword) {
      setPassword(passwordFilds);
    } else {
      reset(email);
    }
    setEmail('');
  };
  const handleEmailChange = e => {
    setEmail(e.target.value);
  };

  const handlePasswordChange = e => {
    setPasswordFilds({
      ...passwordFilds,
      [e.target.name]: e.target.value
    });
  };
  return (
    <Form name="loginForm" size="large" onSubmit={onSubmit}>
      <Segment>
        {resetPassword
          ? (
            <>
              <Form.Input
                fluid
                icon="lock"
                name="firstPassword"
                iconPosition="left"
                placeholder="Password"
                type="password"
                onChange={handlePasswordChange}
              />
              <Form.Input
                fluid
                icon="lock"
                name="secondPassword"
                iconPosition="left"
                placeholder="Repeat Password"
                type="password"
                onChange={handlePasswordChange}
              />
              <Button type="submit" color="teal" fluid size="large" primary>
                Set new Password
              </Button>
            </>
          )
          : (
            <>
              <Form.Input
                fluid
                icon="at"
                iconPosition="left"
                placeholder="Email"
                type="email"
                onChange={handleEmailChange}
              />
              <Button type="submit" color="teal" fluid size="large" primary>
                Send Email
              </Button>
            </>
          )}

      </Segment>

    </Form>

  );
};

PasswordResetForm.propTypes = {
  reset: PropTypes.func.isRequired,
  setPassword: PropTypes.func.isRequired,
  resetPassword: PropTypes.string
};

PasswordResetForm.defaultProps = {
  resetPassword: ''
};

export default PasswordResetForm;
