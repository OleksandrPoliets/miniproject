import React from 'react';
import PropTypes from 'prop-types';
import { List, Loader } from 'semantic-ui-react';
import styles from './styles.module.scss';

const ReactionComponent = ({ userReactionList }) => (
  <div className={styles.reaction}>
    <List>
      {userReactionList.length === 0 && (<Loader size="small" active inline="centered">Loading</Loader>) }
      {userReactionList.map(el => <List.Item key={el.id}>{el.user.username}</List.Item>)}
    </List>
  </div>
);
ReactionComponent.defaultProps = {
  userReactionList: []
};

ReactionComponent.propTypes = {
  userReactionList: PropTypes.arrayOf(PropTypes.object)
};
export default ReactionComponent;
