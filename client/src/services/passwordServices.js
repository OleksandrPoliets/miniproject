import callWebApi from 'src/helpers/webApiHelper';

export const resetPassword = async request => {
  const response = await callWebApi({
    endpoint: '/api/password/reset',
    type: 'put',
    request
  });
  return response.json();
};

export const setPassword = async ({ password, token }) => {
  const response = await callWebApi({
    endpoint: '/api/password/set-password',
    type: 'put',
    request: {
      password,
      token
    }
  });
  return response.json();
};
