import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Redirect, NavLink } from 'react-router-dom';
import PasswordResetForm from 'src/components/PasswordResetForm';
import { Grid, Header, Message } from 'semantic-ui-react';
import Logo from 'src/components/Logo';
import { resetPassword, setNewPassword } from './actions';

const ResetPassword = ({ match }) => {
  const { passwordToken } = match.params;
  const [error, setError] = useState(false);
  const [message, setMessage] = useState('');
  const [messageHeader, setMesageHeader] = useState('');
  const [validateEmail, setValidateEmail] = useState(false);
  const [toMainPage, setToMainPage] = useState(false);

  const linkToReset = async email => {
    const result = await resetPassword({ email });

    if (result.ok) {
      setError(false);
      setMessage('Reset link send to your Email');
      setMesageHeader('Your request was successful');
    } else {
      switch (result.error) {
        case 'User not Found':
          setValidateEmail(true);
          setError(true);
          setMesageHeader('There was some errors with your Email');
          setMessage('Email not Found Please ');
          break;
        case 'Wrong Email':
          setError(true);
          setMesageHeader('There was some errors with your Email');
          setMessage('Wrong Email format');
          break;
        default:
          break;
      }
    }
  };
  const setPassword = async ({ firstPassword, secondPassword }) => {
    const timeToRedirect = 3000;
    if (firstPassword === secondPassword) {
      if (firstPassword.length > 5) {
        await setNewPassword({
          password: firstPassword,
          token: passwordToken
        });
        setError(false);
        setMessage('Your password was successful saved You will be redirect to main page');
        setMesageHeader('Password saved');
        setTimeout(() => {
          setToMainPage(true);
        }, timeToRedirect);
      } else {
        setError(true);
        setMesageHeader('Password Error');
        setMessage('Password length must be more than 6 symbols');
      }
    } else {
      setError(true);
      setMesageHeader('Password Error');
      setMessage('Password does not match');
    }
  };
  return (
    <Grid textAlign="center" verticalAlign="middle" className="fill">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Logo />
        <Header as="h2" color="teal" textAlign="center">
          Reset Your Password
        </Header>
        <PasswordResetForm
          reset={linkToReset}
          setPassword={setPassword}
          resetPassword={passwordToken}
        />
        {message && (
          <Message
            success={!error}
            error={error}
          >
            <Message.Header>{messageHeader}</Message.Header>
            <p>
              {message}
              {error && validateEmail && (<NavLink exact to="/registration">Sign Up!</NavLink>)}
            </p>
          </Message>
        )}
      </Grid.Column>
      {toMainPage && (<Redirect to={{ pathname: '/login' }} />)}
    </Grid>
  );
};

ResetPassword.propTypes = {
  match: PropTypes.objectOf(PropTypes.any).isRequired
};

export default ResetPassword;
