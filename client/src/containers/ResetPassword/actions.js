import * as passwordServices from 'src/services/passwordServices';

export const resetPassword = async email => passwordServices.resetPassword(email);

export const setNewPassword = async email => passwordServices.setPassword(email);
