import React, { useState, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import Cropper from 'react-easy-crop';
import { connect } from 'react-redux';
import { getUserImgLink } from 'src/helpers/imageHelper';
import {
  Grid,
  Image,
  Input,
  Button,
  Icon,
  Form,
  Label,
  Segment,
  Dimmer,
  Loader
} from 'semantic-ui-react';
import * as imageService from 'src/services/imageService';
import { updatePersonalField } from './actions';
import styles from './styles.module.scss';
import getCroppedImg from './cropImage';

const Profile = ({ user, updatePersonalField: statusUpdate }) => {
  const square = { width: 300, height: 300 };
  const [loading, setLoading] = useState(false);
  const [crop, setCrop] = useState({ x: 0, y: 0 });
  const [zoom, setZoom] = useState(1);
  const [rotation, setRotation] = useState(0);
  const [croppedAreaPixels, setCroppedAreaPixels] = useState(null);
  const [imageSrc, setImageSrc] = useState(null);
  const [image, setImage] = useState(getUserImgLink(user.image));
  const [showFild, setShowFild] = useState(true);
  // eslint-disable-next-line no-unused-vars
  const [errorMessage, seterrorMessage] = useState(false);
  const [usersField, setUsersField] = useState({
    status: user.status,
    username: user.username
  });

  const onCropComplete = useCallback((croppedArea, croppedAreaPixel) => {
    setCroppedAreaPixels(croppedAreaPixel);
  }, []);

  useEffect(() => {
    getUserImgLink(user.image);
  }, [user.image]);
  const onChange = e => {
    e.preventDefault();
    setUsersField({
      ...usersField,
      [e.target.name]: e.target.value
    });
  };

  const updateStatus = async e => {
    e.preventDefault();
    try {
      seterrorMessage(false);
      await statusUpdate(usersField);
    } catch (error) {
      seterrorMessage(true);
    }
  };

  const readFile = file => new Promise(resolve => {
    const reader = new FileReader();
    reader.addEventListener('load', () => resolve(reader.result), false);
    reader.readAsDataURL(file);
  });
  const onFileChange = async e => {
    if (e.target.files && e.target.files.length > 0) {
      const file = e.target.files[0];
      const imageDataUrl = await readFile(file);
      setImageSrc(imageDataUrl);
      setShowFild(false);
    }
  };
  const changeImage = async () => {
    try {
      setLoading(true);
      const croppedImage = await getCroppedImg(
        imageSrc,
        croppedAreaPixels,
        rotation
      );
      setImageSrc(null);
      const result = await imageService.uploadImage(croppedImage);
      setLoading(false);
      setShowFild(true);
      await statusUpdate({
        ...usersField,
        imageId: result.id
      });
      setImage(getUserImgLink(result));
    } catch (e) {
      console.log(e);
    }
  };

  const handleChangeZoom = e => {
    setZoom(e.target.value);
  };

  const handleChangeRotation = e => {
    setRotation(e.target.value);
  };

  const closeCropper = () => {
    setImageSrc(null);
    setShowFild(true);
  };

  return (
    <Grid container textAlign="center" style={{ paddingTop: 30 }}>
      <Grid.Column>
        {loading
          ? (
            <Segment className={styles.loaderWrap} circular style={square}>
              <Dimmer active>
                <Loader size="massive">Preparing Files</Loader>
              </Dimmer>
              <Image centered src={image} size="large" circular />
            </Segment>
          )
          : showFild && (<Image centered src={image} size="medium" circular />)}
        <br />
        {imageSrc && (
          <>
            <div className={styles.cropperWrap}>
              <Cropper
                image={imageSrc}
                crop={crop}
                showGrid={false}
                cropShape="round"
                zoom={zoom}
                cropSize={{ width: 200, height: 200 }}
                onCropChange={setCrop}
                onZoomChange={setZoom}
                onCropComplete={onCropComplete}
                rotation={rotation}
              />
            </div>
            <Grid columns="equal">
              <Grid.Row>
                <Grid.Column className={styles.cropperControlWrap}>
                  <Label pointing="right">Zoom</Label>
                  <Input
                    min={1}
                    max={5}
                    onChange={handleChangeZoom}
                    step={0.1}
                    type="range"
                    value={zoom}
                  />
                </Grid.Column>
                <Grid.Column className={styles.cropperControlWrap}>
                  <Form.Input
                    min={0}
                    max={360}
                    name="duration"
                    onChange={handleChangeRotation}
                    step={1}
                    type="range"
                    value={rotation}
                  />
                  <Label pointing="left">Rotate</Label>
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column>
                  <Button
                    color="teal"
                    icon
                    labelPosition="left"
                    onClick={changeImage}
                  >
                    <Icon name="check" />
                    Confirm
                  </Button>
                </Grid.Column>
                <Grid.Column>
                  <Button
                    color="red"
                    icon
                    labelPosition="left"
                    onClick={closeCropper}
                  >
                    <Icon name="cancel" />
                    Cancel
                  </Button>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </>
        )}
        {showFild && (
          <Button
            color="teal"
            icon
            labelPosition="left"
            as="label"
          >
            <Icon name="image" />
            Change profile photo
            <input name="image" type="file" onChange={onFileChange} hidden />
          </Button>
        )}
        <br />
        <br />
        {errorMessage && (
          <Label basic color="red" pointing="below">
            That name is taken!
          </Label>
        )}
        <br />
        <Input
          icon="user"
          iconPosition="left"
          type="text"
          placeholder={usersField.username}
          onChange={e => onChange(e)}
          name="username"
        />
        <br />
        <br />
        <Input
          icon="at"
          iconPosition="left"
          placeholder="Email"
          type="email"
          disabled
          value={user.email}
        />
        <br />
        <br />
        <Input
          icon="pencil alternate"
          iconPosition="left"
          placeholder={usersField.status}
          type="text"
          name="status"
          onChange={e => onChange(e)}
        />
        <br />
        <br />
        {showFild && (
          <Button positive onClick={e => updateStatus(e)}>
            Save Changes
          </Button>
        )}
      </Grid.Column>
    </Grid>
  );
};

Profile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  updatePersonalField: PropTypes.func.isRequired
};

Profile.defaultProps = {
  user: {}
};

const mapStateToProps = rootState => ({
  user: rootState.profile.user
});

const mapDispatchToProps = ({
  updatePersonalField
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
