import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
  ADD_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST,
  SHOW_LIKES,
  EDIT_POST
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

const showLikes = post => ({
  type: SHOW_LIKES,
  post
});

const deletePostAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const editPostAction = posts => ({
  type: EDIT_POST,
  posts
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

export const checkIsLikeOrDislike = async (postId, service) => {
  const { id, prevReaction } = await service(postId);
  return { id: id ? 1 : -1, prevReaction: prevReaction ? -1 : 0 };
};

export const likePost = postId => async (dispatch, getRootState) => {
  const { id: like, prevReaction: dislike } = await checkIsLikeOrDislike(postId, postService.likePost);
  const mapLikes = post => ({
    ...post,
    likeCount: Number(post.likeCount) + like,
    dislikeCount: Number(post.dislikeCount) + dislike
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));
  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapLikes(expandedPost)));
  }
};

export const dislikePost = postId => async (dispatch, getRootState) => {
  const { id: dislike, prevReaction: like } = await checkIsLikeOrDislike(postId, postService.dislikePost);
  const mapLikes = post => ({
    ...post,
    dislikeCount: Number(post.dislikeCount) + dislike,
    likeCount: Number(post.likeCount) + like
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapLikes(expandedPost)));
  }
};

export const getUserLikeList = postId => async (dispatch, getRootState) => {
  const likers = await postService.getLikedUsers(postId);
  const { posts: { posts, expandedPost } } = getRootState();
  const result = posts.map(el => (el.id === postId ? { ...el, likers } : el));

  dispatch(showLikes(result));

  if (expandedPost && expandedPost.id === postId) {
    const expanded = { ...posts.expandedPost, likers };
    dispatch(setExpandedPostAction(expanded));
  }
};

export const deletePostById = postId => async (dispatch, getRootState) => {
  try {
    const reaction = await postService.deletePost(postId);
    if (reaction) {
      const { posts: { posts, expandedPost } } = getRootState();
      const newPosts = posts.filter(el => el.id !== postId);
      dispatch(deletePostAction(newPosts));
      if (expandedPost && expandedPost.id === postId) {
        dispatch(setExpandedPostAction(undefined));
      }
    }
  } catch (err) {
    console.log(err.message);
  }
};

export const editUserPost = (post, postId) => async (dispatch, getRootState) => {
  const editResult = await postService.editPost(post, postId);
  if (editResult.id) {
    const { posts: { posts } } = getRootState();
    const result = posts.map(el => (el.id === editResult.id ? { ...el, ...editResult } : el));
    dispatch(editPostAction(result));
  }
};

export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const sendPostToEmail = (email, link) => postService.sendMail(email, link);
