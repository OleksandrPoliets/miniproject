/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import ExpandedPost from 'src/containers/ExpandedPost';
import Post from 'src/components/Post';
import AddPost from 'src/components/AddPost';
import SharedPostLink from 'src/components/SharedPostLink';
import { Checkbox, Loader } from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import ModalWindow from '../../components/ModalComponent/ModalWindow';
import {
  loadPosts,
  loadMorePosts,
  likePost,
  dislikePost,
  toggleExpandedPost,
  addPost,
  getUserLikeList,
  deletePostById,
  editUserPost,
  sendPostToEmail
} from './actions';

import styles from './styles.module.scss';

const postsFilter = {
  userId: undefined,
  showLiked: undefined,
  from: 0,
  count: 10
};

const Thread = ({
  userId,
  loadPosts: load,
  loadMorePosts: loadMore,
  posts = [],
  expandedPost,
  hasMorePosts,
  addPost: createPost,
  likePost: like,
  dislikePost: dislike,
  toggleExpandedPost: toggle,
  getUserLikeList: likeReactions,
  deletePostById: deleteOwnPost,
  editUserPost: edit
}) => {
  const [sharedPostId, setSharedPostId] = useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = useState(false);
  const [showLikedPosts, setShowLikedPosts] = useState(false);
  const [togleReaction, setTogleReaction] = useState({ show: false, postId: null });
  const [confirmModal, setConfirmModal] = useState(false);
  const [deleteCredentionals, setDeleteCredentioanl] = useState(undefined);
  const [postToEdit, setpostToEdit] = useState(null);

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    postsFilter.userId = showOwnPosts ? undefined : userId;
    postsFilter.showLiked = undefined;
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowLikedPosts = () => {
    setShowLikedPosts(!showLikedPosts);
    postsFilter.showLiked = showLikedPosts ? undefined : userId;
    postsFilter.userId = undefined;
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const getMorePosts = () => {
    loadMore(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const sharePost = id => {
    setSharedPostId(id);
  };

  const uploadImage = file => imageService.uploadImage(file);

  const onReactionBlockHover = async (e, postReaction) => {
    if (e.type === 'mouseenter') {
      if (+postReaction.likeCount > 0) {
        if (!postReaction.likers) {
          likeReactions(postReaction.id);
        }
        setTogleReaction({ show: true, postId: postReaction.id });
      }
    }
    if (e.type === 'mouseleave') {
      setTogleReaction({ show: false, postId: null });
    }
  };

  const onDelete = deletPostId => {
    setConfirmModal(true);
    setDeleteCredentioanl(deletPostId);
  };

  const closeModal = () => {
    setConfirmModal(false);
    setDeleteCredentioanl(undefined);
  };
  const deletePost = () => {
    setConfirmModal(false);
    deleteOwnPost(deleteCredentionals);
  };

  const editPost = postId => {
    setpostToEdit(postId);
  };

  const handleCancelEditPost = () => {
    setpostToEdit(null);
  };

  return (
    <div className={styles.threadContent}>
      {confirmModal && (<ModalWindow close={closeModal} confirm={deletePost} />)}
      <div className={styles.addPostForm}>
        <AddPost addPost={createPost} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          toggle
          label="Show only my posts"
          checked={showOwnPosts}
          onChange={toggleShowOwnPosts}
        />
        <Checkbox
          toggle
          label="Show posts liked by me"
          checked={showLikedPosts}
          onChange={toggleShowLikedPosts}
        />
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Loader active inline="centered" key={0} />}
      >
        {posts.map(post => (
          <Post
            post={post}
            likePost={like}
            dislikePost={dislike}
            toggleExpandedPost={toggle}
            sharePost={sharePost}
            onHover={onReactionBlockHover}
            showLikeList={togleReaction}
            key={post.id}
            userId={userId}
            deletePost={onDelete}
            addPostToEdit={editPost}
            postToEdit={postToEdit}
            uploadImage={uploadImage}
            editUserPost={edit}
            cancelUpdate={handleCancelEditPost}
          />

        ))}
      </InfiniteScroll>
      {expandedPost && (
        <ExpandedPost
          likePost={like}
          dislikePost={dislike}
          sharePost={sharePost}
          showLikeList={togleReaction}
          postToEdit={postToEdit}
          uploadImage={uploadImage}
        />
      )}
      {sharedPostId && (
        <SharedPostLink
          postId={sharedPostId}
          close={() => setSharedPostId(undefined)}
          share={sendPostToEmail}
        />
      )}
    </div>
  );
};

Thread.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object),
  hasMorePosts: PropTypes.bool,
  expandedPost: PropTypes.objectOf(PropTypes.any),
  userId: PropTypes.string,
  loadPosts: PropTypes.func.isRequired,
  loadMorePosts: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  addPost: PropTypes.func.isRequired,
  getUserLikeList: PropTypes.func.isRequired,
  deletePostById: PropTypes.func.isRequired,
  editUserPost: PropTypes.func.isRequired
};

Thread.defaultProps = {
  posts: [],
  hasMorePosts: true,
  expandedPost: undefined,
  userId: undefined
};

const mapStateToProps = rootState => ({
  posts: rootState.posts.posts,
  hasMorePosts: rootState.posts.hasMorePosts,
  expandedPost: rootState.posts.expandedPost,
  userId: rootState.profile.user.id
});

const actions = {
  loadPosts,
  loadMorePosts,
  likePost,
  dislikePost,
  toggleExpandedPost,
  addPost,
  getUserLikeList,
  deletePostById,
  editUserPost
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Thread);
